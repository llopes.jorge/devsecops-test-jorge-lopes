# Task 2: Desenvolvimento de Arquitetura AWS para Aplicativo Web de Alto Desempenho

## Objetivo

Elabore uma arquitetura na AWS para um aplicativo web projetado para lidar com um volume significativo de tráfego. Essa arquitetura deve enfatizar a segurança, escalabilidade e disponibilidade contínua do aplicativo.

## Descrição e Requisitos

O aplicativo web em questão é destinado a um lançamento global e desempenha um papel crucial nas operações do negócio. Espera-se que ele mantenha alta disponibilidade e resiliência, mesmo diante de flutuações imprevisíveis no tráfego. A proteção de dados e a defesa contra ataques externos são prioritárias.

Dentro deste contexto, a arquitetura proposta deve contemplar as seguintes necessidades:

- **Gestão de Domínio:** O aplicativo está associado a um domínio específico. É preciso configurar as entradas de DNS de forma que os usuários possam acessar o aplicativo através de uma URL amigável.
- **Proteção Contra Ataques:** Dada a possibilidade de ataques de negação de serviço (DDoS) e outras ameaças virtuais, medidas preventivas são imprescindíveis.
- **Balanceamento de Carga:** A arquitetura deve incluir uma estratégia eficaz de balanceamento de carga para gerenciar grandes volumes de tráfego, assegurando a distribuição uniforme das solicitações.
- **Orquestração de Contêineres:** Com o aplicativo operando em contêineres, é necessário um sistema de gerenciamento que promova a escalabilidade e a implementação ágil.
- **Gerenciamento de Dados:** Prevê-se um intenso volume de transações de dados. Portanto, a solução de banco de dados relacional (Postgres) deve ser escalável, suportando operações de leitura e escrita de alta velocidade.

## Entregáveis

- **Arquitetura Proposta:** Forneça uma descrição detalhada da arquitetura planejada, justificando a seleção de serviços e tecnologias específicas da AWS para atender a cada requisito.
- **Escalabilidade:** Descreva como a arquitetura proposta irá acomodar automaticamente os picos de tráfego, garantindo a escalabilidade do aplicativo.
- **Medidas de Segurança:** Detalhe as estratégias de segurança que serão implementadas para salvaguardar o aplicativo contra as ameaças digitais mais comuns.
- **Diagrama Arquitetural (Opcional):** Considere incluir um diagrama para ilustrar o fluxo de tráfego e a interação entre os componentes da arquitetura proposta.

Os documentos e artefatos resultantes devem ser comitados no diretório `task2`.
