# Teste para DevSecOps


## Objetivo

Este teste foi elaborado para refletir algumas das atividades que você desempenhará na área de infraestrutura e segurança de nossa empresa. O objetivo é avaliar sua aptidão técnica e capacidade de resolver problemas específicos da função, fornecendo-nos alguns insights para as próximas etapas do processo seletivo.
Encorajamos você a utilizar este momento para demonstrar suas habilidades e conhecimento técnico. Além disso, sinta-se à vontade para fazer quaisquer perguntas que possam surgir durante a resolução das questões. Estamos à disposição para esclarecer dúvidas, visando garantir que você tenha todas as informações necessárias para realizar o teste da melhor maneira possível.

Boa sorte e esperamos ver o melhor de sua capacidade técnica!

Nos diretórios task1 e task2 você vai encontrar um README.md com as instruções de cada task.