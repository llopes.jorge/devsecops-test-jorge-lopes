# Task 1: Pipeline de CI/CD para Spring PetClinic

## Objetivo

O objetivo desta tarefa é criar uma pipeline de integração e entrega contínuas (CI/CD) no GitLab para o projeto Spring PetClinic, um aplicativo de exemplo baseado em Spring Framework. A pipeline deve automatizar testes, construção e implantação do aplicativo, garantindo práticas de segurança desde a integração do código até a entrega no ambiente de produção.

## Requisitos

- Utilizar o GitLab CI/CD criando o .gitlab-ci.yml dentro do diretório task1/spring-petclinic
- Automatizar a compilação e empacotamento
- Automatizar a execução dos testes unitários
- Criar um container para execução do PetClinic e fazer upload para um Container Registry. Pode ser utilizado o Container Registry do GitLab, Docker Hub ou outro de sua escolha.
- (Opcional) Publicar o projeto em uma Cloud, exemplo Heroku.
